# Two Phase Commit
Du kan se prosjektet på gitlab via denne lenken:
https://gitlab.stud.iie.ntnu.no/sebastai/two-phase-commit
## Intro
Denne oppgaven ble gitt som frivillig prosjekt i faget TDAT2004 og omhandler 
å lage en egen implementasjon av Two Phase Commit protokollen. Dette dokumentet er ment
som hjelp til å få oversikt over denne implementasjonen og er delt inn i følgende deler:

- Teori og implementasjon
- Installering
- Testing og API
- Funksjonalitet
- Eksempler

Oppgaven er løst selvstendig og koden/dokumentasjon er skrevet av Sebastian Ikin.
Noe kode er lånt fra tidligere øvinger.
## Teori og implementasjon
Two phase commit er en variant av atomisk forpliktelses algoritme 
for distribuerte systemer. Oversatt til mer folkelig språk er det en kontroll-algoritme
som gjennomfører endringer over et totalt system i faste enheter (derav atomisk).
Enten forplikter alle noder/enheter i et system seg til en endring, eller så
gjennomføres ikke endringen.

Two phase commit består av to typer kommunisatorer, particpant og coordinator.
Coordinatoren er ansvarlig for å koordinere transaksjonen over det distribuerte
systemet. En participant er en deltager i transaksjonen og må stemme ja ved
setup-fasen for at en transaksjon skal gjennomføres.

Denne implementasjonen av two phase commit består kort forklart av en participant
som sender en "commit-request" til en coordinator-server som så triggrer alle
klientene koblet til coordinator-serveren til å gå inn i setup-modus. Her blir
hver enkelt klient en participant som så prøver å gjennomføre endringen og sender
så en stemme tilbake. Om alle har stemt ja, sender coordinator-serveren en suksess
-melding og venter på ACK fra alle klienter. Ved feil (i.e en av participantene 
stemmer nei) vil suksess settes til false i meldingen, og participantene vil reverte
til tilstanden før setup. Når dette er gjort sendes det også ACK til server.  


### Biblotek og språk
I denne oppgaven har jeg valgt å bruke JavaScript, slik at jeg kunne
lage et greit klient-program for å presentere protokollen. Siden jeg endte med å
velge JS var Websocket det naturlige valget for kommunikasjon mellom
server og klient, da det ikke krever eksterne biblotek og at jeg allerede
hadde en implementasjon av handshake-protokoll til denne. Videre kan det 
også sies at når vi kommuniserer over socketen som del av two-phase-commiten
gjøres dette v.i.a JSON-strenger som gjør parsing og håndtering av meldinger lettere enn ellers.

Alternativt kunne man brukt TCP eller UDP-sockets i et annet språk,  men da
hadde vi som sagt ikke kunne vært like fleksibel med klienten. Videre kan det også sies
at det er greit å holde det enkelt når vi kun ønsker å demonstrere protokollen.

Det er brukt minimalt av ekstrene bibloteker og der de er brukt er de ikke del av 
selve gjennomføringen av protokollen. Disse biblotekene er hovedsakelig relatert
til React og Jest.

### Arkitektur
I korte trekk har participanten en lokal verdi og en referanse til en global verdi
som håndteres av coordinator. Bruker på klientsiden kan gjøre lokale endringer
og commiter disse. Ved en godkjent commit vil den globale verdien hos
coordinatoren settes til den nye verdien og synkroniseres over tilkoblede klienter
ved hjelp av two-phase-commit protokollen slik beskrevet over.


Sentralt til systemet er coordinator som kjøres på server. Denne coordinatoren har
en global verdi som synkroniseres med participant ved tilkobling (etter
gjennomført websocket-handshake). 
Grunnen til at vi gjør det på denne måten er for å unngå 
usikkerhet rundt hvem som har den gyldige
globale verdien (altså coordinatoren) og unngå å ha en komplett node-graf av websockets
hos klientene (i.e at vi har n*(n-1) websockets, istedet for n+1). Noe som ville 
vært tilfellte om participanten som commiter hadde blitt coordinator.

## Installering 
For å kunne kjøre programmet må du ha node installert. Du kjører så 
coordinator.js ved hjelp av kommandoene:

```sh
cd coordinator_server
npm install
npm start
```

fra topp-mappen.

For å kjøre klienten benytter du følgene kommandoer fra topp-mappen:

```sh
cd participator_client
npm install
npm start
```

Klienten vil da kjøre over localhost:3000, og du kan besøke klienten her.
Websocketen hos Coordinator-serveren kjøres over localhost:3001.

## Testing og API
### Enkel systemtest

For å teste om meldings-gangen er på plass kan man gjøre en
lokal endring hos en klient og commite denne. Outputen fra prossessen
finner du både hos klient og server.

For å produsere feil kan du gjøre lokale endringer hos to klienter,
og så commite en av de. De lokale endringene vil nå reverseres hos begge.

Disse testene er demonstrert som del av eksempeler lengre ned.

### Enhetstester
Kjør coordinator-testene:

```sh
cd coordinator_server
npm test
```

Kjør participant-testene:
```sh
cd participant_client
npm test
```


### API
Du kan lese API-dokumentasjon her:
https://zantiki.github.io/

## Funksjonalitet
### Implementert funksjonalitet:

- Enkel webklient som også fungerer som participant.
- Egen Coordinator-Server
- 2-fase commit protokoll som synkroniserer en verdi over alle participants
- Reconnect funksjonalitet som synkroniserer coordinator-verdier med participant
- Websocket handshake ved tilkobling av ny participant.

### Fremtidig funksjonalitet:
Slik protokollen er implementert nå, er den ikke spesielt praktisk til bruk, 
da to klienter ikke kan ha forskjellige lokale 
verdier dersom en ønsker å commite. En løsning kan da være å legge til en "commit-kø"
av noe slag.

Alternativt kan man også finne en måte å sette participanten som commiter
til coordinator. Dette er allikvel litt utforedrende da dette betyr at 
alle noder må være tilkoblet hverandre for å sørge for at global verdi er
synkronisert. 

Av praktiske hensyn kunne det også vært hensiktsmessig å sende output fra
coordinator-server til klienten slik at man 
fikk demonstrert protokollen på ett sted. Dette kan eventuelt gjøres med
en egen "info-socket" som oppdaterer klienten med info fra server.

## Eksempler
### Godkjent commit, en participant
Participant før commit:
![](docs/examples/client11.png)

Participant etter å ha commitet 3:
![](docs/examples/client12.png)

Coordinator før commit:
![](docs/examples/server1client1.png)

Coordinator etter commit (Eneren du ser er antall participants i transaksjonen):
![](docs/examples/server2client1.png)
### Godkjent commit, to participants
Setup og output her ser likt ut hos participant-clienten som i eksempelet over.
Det samme gjelder handshake hos coordinator, hovedforskjellen er output ved commit hos
coordinator. Denne ser du under (Toeren er antall participants i transaksjonen).
![](docs/examples/server2client2.png)

### Feilet commit, to participants

Vi setter opp to participants med forskjellige local-value:
![](docs/examples/client22.png)

![](docs/examples/client21.png)

Ved commit hos en av participantsene vil coordinator se slik ut:
![](docs/examples/serverErr.png)

Participant-ene vil da reverte local-value og gi følgende output:
![](docs/examples/cli1Err.png)

![](docs/examples/cli2Err.png)

Participanten som stemte nei, er da den som ikke startet transaksjonen.
##
Skrevet av Sebastian Ikin, 20.04.2020


