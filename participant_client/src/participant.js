/**
 * Class representing a log, containing forwards and backwards changes,
 * as well as system events.
 */
class Log{
    /**
     * Undo log, part of two-phase commit
     * @type {Array}
     */
    undo = [];
    /**
     * Redo log, part of two-phase commit
     * @type {Array}
     */
    redo = [];
    /**
     * Eventlog to use client side.
     * @type {Array}
     */
    eventLog = [];

    /**Appends an action to the undo log*/
    addUndo(action){
        this.undo.push(action);
    }
    /**Appends an action to the redo log*/
    addRedo(action){
        this.redo.push(action);
    }

    /**Appends an action to the event log*/
    addEvent(ev){
        this.eventLog.push(ev);
    }
}

/**
 * Class describing a participant entity,
 * contains a websocket and log object.
 */
export class Participant {
    /**
     * Refrence to coordinator global-value
     */
    globalValue;
    /**
     * Local value to commit, changeable through onValue method.
     */
    localValue = 0;
    /**
     * Log object containing undo, redo and eventlog.
     */
    participantLog = new Log();
    /**
     * Websocket for server communication
     */
    ws;
    /**
     * Boolean value for checking if participant
     * is responsible for current commit.
     */
    isInitialiser = false;

    /**Represents a participant
     * @constructor
     */
    constructor(){
        this.ws = new WebSocket('ws://localhost:3001');
        this.setSocketHandlers();
    }

    /**Sends the value to commit to coordinator.*/
    commit(newValue){
        this.isInitialiser = true;
        console.log(newValue);
        this.ws.send(JSON.stringify({COMMIT: newValue}));
    }

    /**Appends a string to the eventlog and calls theoverridable onLog method.*/
    commitError(){
        console.log("Error");
        this.ws.send(JSON.stringify({VOTE: "NO"}));
        this.log("Error occurred, voted no");
    }

    /**Appends a string to the eventlog and calls the overridable onLog method.*/

    log(log){
        this.participantLog.addEvent(log);
        this.onLog();
    }

    /**Resets the websocket and socket handlers.*/

    reconnect(){
        console.log("Closing socket");
        setTimeout(() => {
            this.ws = new WebSocket('ws://localhost:3001');
            this.setSocketHandlers();
        }, 1500);
    }


    /**
     * helper method that sets all the necessary handler methods to the globally scoped
     * websocket.
     */

    setSocketHandlers(){
        this.ws.onopen = event => {
            this.log("Connected to coordinator");
        };

        this.ws.onclose = ()  => {
            //this.ws = new WebSocket('ws://localhost:3001');
            this.log("Disconnected");
        };

        this.ws.onerror = event => {
            this.log("Socket error");
        }
        this.ws.onmessage = event => {
            console.log(event.data);
            let data = JSON.parse(event.data);

            //Set global value on connect
            console.log(data);
            if(data.GLOBAL_VALUE !== undefined){
                this.globalValue = data.GLOBAL_VALUE;
                this.localValue = data.GLOBAL_VALUE;
                this.participantLog.addUndo(this.globalValue);
                this.onNewValue();
            }


            if(data.SETUP != undefined){
                this.log("Setting up...");
                this.participantLog.addUndo(this.globalValue);
                this.participantLog.addRedo(data.SETUP);
                if(this.localValue !== this.globalValue && !this.isInitialiser) {
                    this.commitError();
                    this.log("Local variable not synced")
                }else{
                    console.log("Setup");
                    try{
                        this.globalValue = data.SETUP;
                        this.ws.send(JSON.stringify({VOTE: "YES"}));
                        this.log("Set up successful, voted yes");
                        console.log("Vote");
                    }catch (e) {
                        this.ws.send(JSON.stringify({VOTE: "NO"}));
                        this.log("Error occurred, voted no");
                    }
                }
            }else if(data.SUCCESS === false){
                this.globalValue = this.participantLog.undo[this.participantLog.undo.length-1];
                this.localValue = this.participantLog.undo[this.participantLog.undo.length-1];

                console.log(this.participantLog.undo);
                this.onNewValue();
                this.ws.send(JSON.stringify({ACK: true}));
                this.log("Commit failed");
                this.isInitialiser = false;
            } else if(data.SUCCESS) {
                this.ws.send(JSON.stringify({ACK: true}));
                this.isInitialiser = false;
                this.localValue = this.globalValue;
                this.log("Commit successful");
                this.onNewValue();
                console.log("success");
                console.log("Current value: "+this.globalValue);
            }
        };
    }

    /**
     * Overridable method that can be implemented in react component.
     */
    onLog;
    /**
     * Overridable method that can be implemented in react component.
     */
    onNewValue;
}




