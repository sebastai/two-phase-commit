
import * as React from 'react';
import {Route, BrowserRouter} from 'react-router-dom';
import ReactDOM from 'react-dom';
import {createBrowserHistory} from "history";
import {Component} from "react";
import {Participant} from "./participant";

const history = createBrowserHistory();

export class Home extends Component{
    state;

    setInputValue(value){
        console.log(value);
        this.state.participant.localValue = value;
        this.mounted();
    }

    constructor(props){
        super(props);
        let p = new Participant();
        this.state = {
            participant: p,
            log: ["Connection error :'("],
            inputValue: 0
        };

        this.state.participant.onLog = () => {
            console.log("Log called");
            this.mounted();
        }

        this.state.participant.onNewValue = () => {
            this.mounted();
        }
    }

    render() {
        return(
            <div className={"container"}>
            <div style={{border: "2px solid-black", borderStyle: "double", marginTop: "30px", paddingTop: "10px", paddingBottom: "10px"}} >
                <div className={"row"} style={{display: "flex", justifyContent: 'center', alignItems: 'center'}} >
                    <h1>Participant Client</h1>
                </div>
                <div className={"row"} style={{display: "flex", justifyContent: 'center', alignItems: 'center'}} >
                    <p>Basic implementation of a participant, part of the two-phase-commit protocol.
                        Will produce error on discrepancy between local and global state.</p>
                    <button className={"btn btn-primary"} onClick={this.reconnect.bind(this)}>RECONNECT TO COORDINATOR</button>
                </div>
                <div className={"row"} style={{display: "flex", justifyContent: 'center', alignItems: 'center', marginTop: "10px"}}>
                    <div className={"column-6"}>
                        <h1> Global: {this.state.participant.globalValue}</h1>
                        <h1 id={"local_value"}> Local: {this.state.participant.localValue}</h1>
                        <input type={"number"} onChange={event => {this.setInputValue(event.target.value)}}></input>
                        <div className={"row"} style={{display: "flex", justifyContent: 'center', alignItems: 'center', marginTop: "10px"}}>
                            <button className={"btn btn-primary"} onClick={this.commit.bind(this)}>COMMIT</button>
                        </div>
                    </div>
                    <div className={"column-6"}>
                        <div style={{ minWidth: "400px", height: "200px", border: "3px solid-grey", overflow: "auto", backgroundColor: "black" }}>
                            {this.state.log.map(line => (
                                    <p style={{marginLeft: "5px", color: "green"}}>
                                        {"output> "+line}
                                    </p>
                                )
                            )}
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }

    mounted(){
        console.log(this.state.participant.participantLog.eventLog);
        let update = {
            log: this.state.participant.participantLog.eventLog,
            inputValue: this.state.participant.value
        };
        this.setState(update);
    }

    reconnect(){
        this.state.participant.reconnect();
    }

    commit(){
        this.state.participant.commit(this.state.participant.localValue);
    }

}

const root = document.getElementById('root');
if (root)
    ReactDOM.render(
    <BrowserRouter history={history}>
    <Route exact path="/" component={Home}/>
</BrowserRouter>,
root
);