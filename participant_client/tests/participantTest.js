import * as React from 'react';
import { Component } from 'react-simplified';
import { Home } from "../src/index.js";
import { shallow, mount } from 'enzyme';

describe('Component tests', () => {

    it('OnLog', done => {
        const wrapper = shallow(<Home/>);
        let instance = wrapper.instance();
        if (instance){
            instance.state.participant.log("test");
            expect(instance.state.log.length).toBe(1);
        }
        done();
    });

    it('OnValue', done => {
        const wrapper = shallow(<Home/>);
        let instance = wrapper.instance();
        if(instance){
            instance.state.participant.localValue = 20;
            instance.state.participant.onNewValue();
            expect(instance.state.participant.localValue).toBe(20);
        }
        done();
    });
});