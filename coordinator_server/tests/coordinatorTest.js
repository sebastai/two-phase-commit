let c  = require("../coordinator");

describe("Coordinator method tests", () => {
    let coordinator = c.coordinator;

    it("encode", done => {
        let encoded = coordinator.encode("test");
        expect(encoded instanceof Buffer).toBeTruthy();
        done();
    });
    it("decode", done => {
        let decoded = coordinator.decode(coordinator.encode("test"));
        expect(decoded).toBe("VVes\"t");
        done();
    });
    it("Generate Accept Value", done => {
        let acceptValue = coordinator.generateAcceptValue("1234");
        expect(typeof acceptValue === "string" ).toBeTruthy();
        done();
    });

});