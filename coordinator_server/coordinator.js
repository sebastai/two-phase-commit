
const net = require("net");
const crypto = require("crypto");

/**
 * Class describing coordinator, part of two-phase commit.
 */
class Coordinator {

    constructor(){
        this.clients = [];
        this.tempClients = [];
        this.globalValue = 1;
        this.wasSuccess = false;
        this.PORT = 3001;

        this.server = net.createServer(conn => {
            console.log("Client connected");

            conn.on("data", data => {
                console.log('Data from client:\n' + data.toString());

                //Check if get, and do handshake
                if (data.toString()[0] == "G") {
                    let key = data.toString().substring(data.toString().indexOf("-Key: ") + 6,data.toString().indexOf("==") + 2);
                    let acceptValue = this.generateAcceptValue(key);

                    const responseHeaders = [
                        "HTTP/1.1 101 Web Socket Protocol Handshake",
                        "Upgrade: websocket",
                        "Connection: Upgrade",
                        "Sec-WebSocket-Accept:" + acceptValue
                    ];
                    //Send handshake-res to client
                    conn.write(responseHeaders.join("\r\n") + "\r\n\r\n");
                    conn.write(this.encode({GLOBAL_VALUE: this.globalValue}));
                    this.clients.push(conn);
                } else {
                    try{
                        data = JSON.parse(this.decode(data));
                    }catch(err){
                        console.log(conn);
                        const index = this.clients.indexOf(conn);
                        if (index > -1) {
                            this.tempClients.splice(index, 1);
                        }
                        //conn.ws.close();
                    }
                    if(data.COMMIT != undefined){
                        this.tempClients = [...this.clients];
                        this.tempValue = data.COMMIT;
                        this.notifyAll({SETUP: data.COMMIT})
                    }else if(data.VOTE){
                        if(data.VOTE === "YES"){
                            console.log("Got vote");
                            const index = this.tempClients.indexOf(conn);
                            if (index > -1) {
                                this.tempClients.splice(index, 1);
                            }
                        }else{
                            console.log("vote failure");
                            this.notifyAll({SUCCESS: false})
                        }

                        if(this.tempClients.length == 0){
                            console.log("vote success");
                            this.wasSuccess  = true;
                            this.tempClients = [...this.clients];
                            this.notifyAll({SUCCESS: true})
                        }
                    }else if(data.ACK){
                        console.log("Got ACK Success");
                        const index = this.tempClients.indexOf(conn);
                        if (index > -1) {
                            this.tempClients.splice(index, 1);
                        }
                        if(this.tempClients.length === 0 && this.wasSuccess){
                            console.log("transaction successful");
                            this.globalValue = this.tempValue;
                            this.wasSuccess = false;
                        }else if(this.tempClients.length === 0){
                            console.log("Transaction failure");
                        }
                    }

                }
            });

            conn.on("end", () => {
                console.log("Client disconnected");
                const index = this.clients.indexOf(conn);
                if (index > -1) {
                    this.clients.splice(index, 1);
                }
            });
        });

        this.server.on("error", error => {
            console.error("Error: ", error);
        });

        this.server.listen(3001, () => {
            console.log("WebSocket server listening on port "+ 3001 );
        });
    }

    /**
     * Generate accept value in accordance to Websocket protocol.
     *
     * @param: {String} acceptKey
     * @return: {String} acceptValue
     */
    generateAcceptValue(acceptKey) {
      return crypto
            .createHash("sha1")
            .update(acceptKey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11")
            .digest("base64");
    }



    /**
     *Decode a given char-code
     *
     * @param: {String} data - message to decode
     * @return: {String} message - the decoded message
     */
    decode (data){
        let message = "";
        let length = data[1] & 127;
        let maskStart = 2;
        let dataStart = maskStart + 4;

        for (let i = dataStart; i < dataStart + length; i++) {
            let byte = data[i] ^ data[maskStart + ((i - dataStart) % 4)];
            message += String.fromCharCode(byte);
        }
        console.log("Message reads: "+message);
        return message;
    }

    /**
     *Encode a given JSON message
     *
     * @param: {JSON} message - message to encode
     * @return: {Buffer} buffer - message encoded as buffer
     */
    encode(message){
        let msg = JSON.stringify(message);
        let buffer = Buffer.concat([
            new Buffer.from([
                0x81,
                "0x" +
                (msg.length + 0x10000)
                    .toString(16)
                    .substr(-2)
                    .toUpperCase()
            ]),
            Buffer.from(msg)
        ]);
        return buffer;
    }
    /**
     * Sends a message to alle the clients in the client array
     * removes faulty connections
     *
     * @param: {JSON} msg - message to send to clients
     */
    notifyAll(msg){
        let message = this.encode(msg);
        console.log(this.clients.length);
        this.clients.forEach(client => {
            try{
                client.write(message);
            }catch{
                console.log("An error occured");
                const index = this.clients.indexOf(client);
                if (client > -1) {
                    this.clients.splice(index, 1);
                }
                //client.terminate();
            }
        });
    }
}

// Init and run server.
let coordinator = new Coordinator();
module.exports = {coordinator};
